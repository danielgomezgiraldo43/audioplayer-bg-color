Vista antes de reproducir:

![alt text](./assets/sc1.PNG "Antes")

Vista mientras reproduce:

![alt text](./assets/sc2.PNG "Durante")

Controles:

| Botón | Función |
| ------ | ------ |
| A | Play |
| B | Pause |
| Right | Adelantar canción 5 segundos |
| Left | Rebobinar canción 5 segundos |
| Up | Subir Volumen |
| Down | Bajar Volumen |