const playBtn = document.querySelector("#carcasa #botones1 #btnA");
const pauseBtn = document.querySelector("#carcasa #botones1 #btnB");
const volUpBtn = document.querySelector("#carcasa #botones1 #volUp");
const volDownBtn = document.querySelector("#carcasa #botones1 #volDown");
const controls = {
    play: playBtn,
    pause: pauseBtn,
    volUp: volUpBtn,
    volDown: volDownBtn
}
const info = {
    song: "Wildest Dreams",
    artist: "Joseph Black"
}
const player = new AudioPlayer("./assets/songs/1.mp3", document.querySelector("#carcasa"),controls,info);