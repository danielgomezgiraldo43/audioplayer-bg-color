class AudioPlayer{
    constructor(src,domElement,controls,info){
        this.src = src;
        this.audio = new Audio(this.src);
        this.controls = controls;
        this.isPaused = true;
        this.domElement = domElement;
        this.progress = this.domElement.querySelector(".progress");
        this.songName = info.song;
        this.artistName = info.artist;

        this.initControls();
        this.initProgressActions();
        this.audio.ontimeupdate = () => {this.updateUI()}
        this.initTime();
    }

    initControls(){
        if(this.controls.play){
            this.initPlay(this.controls.play);
        }
        if(this.controls.pause){
            this.initPause(this.controls.pause);
        }
        if(this.audio.volume >= 0 && this.audio.volume <= 100){
            this.initVolume(this.controls.volUp, this.controls.volDown);
        }
    }

    initVolume(volUp,volDown){
        volUp.onclick = () =>{
            try{
                this.audio.volume += 0.1;
                console.log(this.audio.volume)
            }
            catch {
                this.audio.volume = 1;
            }
        }
        volDown.onclick = () =>{
            try{
                this.audio.volume -= 0.1;
                console.log(this.audio.volume)
            }
            catch{
                this.audio.volume = 0;
            }
        }
    }

    initPlay(domElement){
        domElement.onclick = () => {
            if(this.isPaused){
                this.play();
                this.isPaused = false;
            }
        }
    }

    initPause(domElement){
        domElement.onclick = () => {
            if(!this.isPaused){
                this.pause();
                this.isPaused = true;
            }
        }
    }

    initProgressActions(){
        const back = this.domElement.querySelector("#botones1 #back");
        const forth = this.domElement.querySelector("#botones1 #forth");
        back.onclick = (e) => {
            this.backwardTime();
        }
        forth.onclick = (e) => {
            this.forwardTime();
        }
    }

    initTime(){
        const duration = this.domElement.querySelector("#display #duration");
        const current = this.domElement.querySelector("#display #current");
        duration.innerHTML = '-:--';
        current.innerHTML = '0:00';
    }

    calcTime(time){
        let hr = ~~(time / 3600);
        let min = ~~((time % 3600) / 60);
        let sec = ~~(time % 60);
        let sec_min = "";
        if (hr > 0) {
           sec_min += "" + hrs + ":" + (min < 10 ? "0" : "");
        }
        sec_min += "" + min + ":" + (sec < 10 ? "0" : "");
        sec_min += "" + sec;
        return sec_min;
    }

    forwardTime(){
        this.audio.currentTime += 5;
    }

    backwardTime(){
        this.audio.currentTime -= 5;
    }

    play() {
        this.audio.play().then().catch(err => console.log(`Error al reproducir el archivo: ${err}`));
    }

    pause() {
        this.audio.pause();
    }

    updateUI(){
        // console.log(this.audio.currentTime);
        const total = this.audio.duration;
        const current = this.audio.currentTime;
        const progress = (current / total) * 100;
        this.progress.style.width = `${progress}%`;
        const showDuration = this.domElement.querySelector("#display #duration");
        const showCurrent = this.domElement.querySelector("#display #current");
        showDuration.innerHTML = this.calcTime(total);
        showCurrent.innerHTML = this.calcTime(current);
        const song = this.domElement.querySelector("#display #songname");
        const artist = this.domElement.querySelector("#display #artistname");
        song.innerHTML = this.songName;
        artist.innerHTML = this.artistName;
    }

}